# Rough notes
* https://developers.google.com/web/progressive-web-apps/
* Flipkart replaced native with progressive web app
* Facebook web forces you to use Messenger app
* You can scan credit cards with mobile web
* Chrome can do notifications on Android. Can Safari on iOS?
* Using manifest.json for theming/icon
* Offline-first
* What can't be done? Yet.
* How is this like web v. desktop, how is it different?

## Walled gardens
* Apple's cut of payments
* Apple's developer fees
* Approval process
* Customer's may feel safer and find it easier to use

## Developers, developers, developers
* If you let them build it, they will come
* Microsoft did well by attracting developers in the 90's and 00's.  Oddly, the one-size-fits-all approach Windows took to accommodate mobile is what drove me away.
* Now most developers seem to use Macbooks.
